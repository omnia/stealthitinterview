####Run the app 
```
sbt run
```

####Run tests
```
sbt test
```

####Assumptions

- the input will be like this `Apple, Orange, Apple
` I don't care if the fruits names is capital or not or if there are whitespace between `,` 
- `,` should be between fruits names
- The only input validation I implement is if the user enter an unrecognised fruit