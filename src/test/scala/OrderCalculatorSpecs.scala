import checkout._
import org.scalatest._
import model.{Apple, Orange}

class OrderCalculatorSpecs extends FlatSpec with Matchers {
  "OrderCalculaor" should "return the total price with no offers" in {
    val order: Order = Right(List(Apple(), Orange(), Apple()))
    val price = OrderCalculator.apply(order)

    price.right.get should be (1.45)
  }

  it should "apply half price discounts if buy1get1free offer on apples is active" in {
    val order: Order = Right(List(Apple(), Orange(), Apple()))
    val activeOffers: Seq[Offer] = Seq(Buy1Get1Free(Apple()))

    val price = OrderCalculator.apply(order, activeOffers)

    price.right.get should be (0.85)
  }

  it should "apply buy1get1free discounts only on even numbers of apples and keep the extra one" in {
    val order: Order = Right(List(Apple(), Apple(), Orange(), Apple()))
    val activeOffers: Seq[Offer] = Seq(Buy1Get1Free(Apple()))

    val price = OrderCalculator.apply(order, activeOffers)

    price.right.get should be (1.45)
  }

  it should "apply discount if buy3pay2 offer is active on oranges" in {
    val order: Order = Right(List(Apple(), Orange(), Orange(), Orange()))
    val activeOffers: Seq[Offer] = Seq(Buy3Pay2(Orange()))

    val price = OrderCalculator.apply(order, activeOffers)

    price.right.get should be (1.1)

  }

  it should "apply multip discounts if we have multiple eligible offers" in {
    val order: Order = Right(List(Apple(), Apple(), Orange(), Orange(), Orange()))
    val activeOffers: Seq[Offer] = Seq(Buy3Pay2(Orange()), Buy1Get1Free(Apple()))

    val price = OrderCalculator.apply(order, activeOffers)

    price.right.get should be (1.1)

  }
}
