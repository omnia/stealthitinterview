import formate_input.ReadFromInputs
import org.scalatest._
import model.{Apple, Orange}

class ReadFromInputsSpecs extends FlatSpec with Matchers {

  "getFruits" should "return list of fruits if the fruits are apple and orange" in {
    val results = ReadFromInputs.getFruits("Apple, Orange")
    val fruits = results.right.get
    fruits.size should be(2)
    fruits(0).isInstanceOf[Apple] should be(true)
    fruits(1).isInstanceOf[Orange] should be(true)
  }
  it should "throw execption if the input contains anything else than apple or orange" in {
    val results = ReadFromInputs.getFruits("Apple, Bannan")
    results.left.get.getMessage should be ("Don't have 'Bannan' in our store")
  }
}
