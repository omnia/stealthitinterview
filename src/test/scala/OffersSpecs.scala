import checkout.{Buy1Get1Free, Buy3Pay2}
import model.{Apple, Orange}
import org.scalatest._

class OffersSpecs  extends FlatSpec with Matchers {

  "Buy1Get1Free" should "apply half price discount on eligible fruits in the order" in {
    val buy1Get1Free = new Buy1Get1Free(Orange())
    val fruits = List(Apple(), Orange(), Orange())

    buy1Get1Free.discount(fruits) should be (0.25)
  }

  it should "not apply any discount on if the eligible fruits less than 1" in {
    val buy1Get1Free = new Buy1Get1Free(Orange())
    val fruits = List(Apple(), Orange())

    buy1Get1Free.discount(fruits) should be (0.0)
  }

  it should "apply the discount only on even numbers of fruits" in {
    val buy1Get1Free = new Buy1Get1Free(Orange())
    val fruits = List(Orange(), Orange(), Orange())

    buy1Get1Free.discount(fruits) should be (0.25)
  }

  "Buy3Pay2" should "apply discount on eligible fruits in the order" in {
    val buy3Pay2 = new Buy3Pay2(Orange())
    val fruits = List(Orange(), Orange(), Orange())

    buy3Pay2.discount(fruits) should be (0.25)
  }

  it should "not apply discount if the eligible fruits in the order less than 3" in {
    val buy3Pay2 = new Buy3Pay2(Orange())
    val fruits = List(Orange(), Orange())

    buy3Pay2.discount(fruits) should be (0.0)
  }

  it should "apply discount only on the correct number of eligible fruits in the order" in {
    val buy3Pay2 = new Buy3Pay2(Orange())
    val fruits = List(Orange(), Orange(), Orange(),
                      Orange(), Orange(), Orange(),
                      Orange())

    buy3Pay2.discount(fruits) should be (0.5)
  }

}
