package checkout
import model.Fruits

object OrderCalculator {
  def apply(order: Order, offers: Seq[Offer] = Seq(NonOffer)): Either[Exception, Double] =
    order match {
    case Left(exception) => Left(exception)
    case Right(fruits) => {
      val discounts: Double = offers.map(_.discount(fruits)).fold(0.0)(_+_)
      val finalPrice = BigDecimal(priceOfFruits(fruits) - discounts)
        .setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble
      Right(finalPrice)
    }
  }

  def priceOfFruits(fruits: List[Fruits]):Double = fruits.map(_.price).fold(0.0)(_ + _)

}
