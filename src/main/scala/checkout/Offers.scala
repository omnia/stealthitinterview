package checkout

import model.{Apple, Fruits, Orange}

sealed trait Offer {
  def discount(fruits: List[Fruits]): Double
}

final case class Buy1Get1Free(on: Fruits) extends Offer {
  override def discount(fruits: List[Fruits]): Double = {
    val eligibleFruits = fruits.filter(_.name == on.name)
    eligibleFruits match {
      case Nil => 0.0
      case f if (f.size.toDouble / 2 == 0 || f.size == 2) =>
        OrderCalculator.priceOfFruits(f) / 2
      case f if (f.size > 1) =>
        ( OrderCalculator.priceOfFruits(f.drop(1)) / 2 )
      case _ => 0.0
    }
  }
}

final case class Buy3Pay2(on:Fruits) extends Offer {
  override def discount(fruits: List[Fruits]): Double = {
    val eligibleFruits = fruits.filter(_.name == on.name)
    eligibleFruits match {
      case Nil => 0.0
      case f => ((f.size / 3).toInt) * on.price
    }
  }
}

final case object NonOffer extends Offer {
  override def discount(fruits: List[Fruits]): Double = 0.0
}

object Offers {
  val activeOffers: Seq[Offer] = Seq(
      new Buy1Get1Free(Apple()),
      new Buy3Pay2(Orange())
  )
}
