import model.Fruits

package object checkout {
  type Order = Either[Exception, List[Fruits]]
}
