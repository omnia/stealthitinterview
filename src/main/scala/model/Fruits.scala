package model

sealed trait Fruits {
  def name: String
  def price: Double
}

final case class Apple(price: Double = 0.6) extends Fruits {
  override val name: String = "Apple"
}

final case class Orange(price: Double = 0.25) extends Fruits {
  override val name: String = "Orange"
}
