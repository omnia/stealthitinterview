import checkout.{Order, OrderCalculator, Offers}
import formate_input.ReadFromInputs

object Checkout extends App {
  println("Enter your order")

  private val order: Order = ReadFromInputs.getFruits(ReadFromInputs.read())

  OrderCalculator.apply(order, Offers.activeOffers) match {
    case (Right(price)) => println(price)
    case (Left(exception)) => throw exception
  }
}
