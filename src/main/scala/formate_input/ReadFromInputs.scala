package formate_input

import java.io.InputStream

import model.{Apple, Fruits, Orange}


object ReadFromInputs {
  def read(input:InputStream = System.in):String = scala.io.StdIn.readLine

  private def string2Fruit(str:String):Either[Exception, Fruits] = str.toLowerCase match {
    case "apple" => Right(Apple())
    case "orange" => Right(Orange())
    case _ => Left(new Exception(s"Don't have '$str' in our store"))
  }

  def getFruits(input: String):Either[Exception, List[Fruits]] =
    input
      .replace(" ", "")
      .split(",")
      .map(string2Fruit)
      .foldLeft[Either[Exception, List[Fruits]]](Right(Nil)) {
        case (Left(exception), _) => Left(exception)
        case (_, Left(exception)) => Left(exception)
        case (Right(fruits), Right(fruit)) => Right(fruits:+fruit)
      }
}
